import random


def is_prime(n):
    """
    Miller-Rabin primality test.

    A return value of False means n is certainly not prime. A return value of
    True means n is very likely a prime.
    """
    if n != int(n):
        return False
    n = int(n)
    # Miller-Rabin test for prime
    if n == 0 or n == 1 or n == 4 or n == 6 or n == 8 or n == 9:
        return False

    if n == 2 or n == 3 or n == 5 or n == 7:
        return True
    s = 0
    d = n - 1
    while d % 2 == 0:
        d >>= 1
        s += 1
    assert (2 ** s * d == n - 1)

    def trial_composite(a):
        if pow(a, d, n) == 1:
            return False
        for i in range(s):
            if pow(a, 2 ** i * d, n) == n - 1:
                return False
        return True

    for i in range(8):  # number of trials
        a = random.randrange(2, n)
        if trial_composite(a):
            return False
    return True

def find_generator(q, p):
    for i in range (20):
        i += 1
        if pow(i, 1, p) == 1 or pow(i, 2, p) == 1 or pow(i, q, p) == 1:
            continue
        elif pow(i, p-1, p) == 1:
            return i

n = 2649376219191757686333291073027588009793925231566294290337286424331787812414080960960191670815548639

print('n is prime:', is_prime(n))
q = (n-1)//2
#print(q)
print('n is safe prime:', is_prime(q))

order = find_generator(q, n)
print('full period generator:', order)
